public class Sim{
    private String name;
    private int mood;
    private static int population = 0;
    
    public Sim(String name){
        this.name = name;
        this.mood = 20;
        population++;
    }

    public static int getPopulation(){
        return population;
    }

    public void increaseMood(){
        this.mood += 5;
    }

    public void decreaseMood(){
        this.mood -= 5;
    }

    public String greeting(){
        String str = "Hello, I'm " + this.name + ".\n";
        if(this.mood < 20){
            str += "And I'm devastated.";
        } else if(this.mood >= 20 && this.mood < 40){
            str += "And I'm sad.";
        } else if(this.mood >= 40 && this.mood < 60){
            str += "And I'm ambivalent.";
        } else if(this.mood >= 60 && this.mood < 80){
            str += "And I'm happy.";
        } else if(this.mood >= 80 && this.mood < 100){
            str += "And I'm ecstatic!";
        }
        return str;
    }
}
