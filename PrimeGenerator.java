import java.util.Scanner;
public class PrimeGenerator{
    public static void main(String[] args){
        int limit;
        Scanner inputReader = new Scanner(System.in);
        int numPrimes = 0;

        System.out.print("enter limit: ");
        limit = inputReader.nextInt();
        System.out.println(limit);

        for(int potentialPrime = 2; potentialPrime <= limit; potentialPrime++){
            // test if the potentialPrime is prime
            boolean isPrime = true;
            for(int potentialDivisor = 2; potentialDivisor <= (0.5*potentialPrime); potentialDivisor++){
                if(potentialPrime % potentialDivisor == 0){
                    isPrime = false;
                    break;
                }
            }

            if(isPrime){
                numPrimes++;
                System.out.printf("%d\t",potentialPrime);
                if(numPrimes % 10 == 0){
                    System.out.println();
                }
            }
        }
    }
}
