/*
 * This class will act like a time stamp. once constructed, it cannot be changed.
 * setTime() will be a private method.
*/
public class Time2{
    //private final int HOUR ;
    //private final int MINUTE;
    //private final int SECOND;
    private final int SECONDS_PAST_MIDNIGHT;

    public Time2(int hour, int minute, int second){
        int seconds = 0;
        boolean validTime = true;

        if(hour >= 0 && hour <= 23){
            seconds = hour*3600;
        } else {
            System.out.println("invalid hour, setting to 0");
            validTime = false;
        }

        if(minute >= 0 && minute <= 59){
            seconds += minute*60;
        } else {
            System.out.println("invalid minute, setting to 0");
            validTime = false;
        }

        if(second >= 0 && second <= 59){
            seconds += second;
        } else {
            System.out.println("invalid second, setting to 0");
            validTime = false;
        }

        if( validTime ){ //the time is valid
            this.SECONDS_PAST_MIDNIGHT = seconds;
        } else {
            this.SECONDS_PAST_MIDNIGHT = 0;
        }
    }

    public Time2(){
        this.SECONDS_PAST_MIDNIGHT = 0;
    }

    public int getHour(){
        return this.SECONDS_PAST_MIDNIGHT/3600;
    }

    public int getMinute(){
        return (this.SECONDS_PAST_MIDNIGHT - getHour()*3600) / 60;
    }

    public int getSecond(){
        return this.SECONDS_PAST_MIDNIGHT - getHour()*3600 - getMinute()*60;
    }

    public String toString(){
        String state = getHour() + ":"+getMinute()+":"+getSecond();
        return state;
    }
}
