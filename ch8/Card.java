public class Card {
    private final String FACE;
    private final String SUIT;

    public Card(String face, String suit){
        this.FACE = face;
        this.SUIT = suit;
    }

    public String toString(){
        return this.FACE + " of " + this.SUIT;
    }
}
