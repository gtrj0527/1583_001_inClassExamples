import java.util.Scanner;

public class SwitchExample{
    public static void main(String[] args){
        Scanner inputReader = new Scanner(System.in);
        int grade;
        
        System.out.print("Enter a number between 0 & 100: ");
        grade = inputReader.nextInt();

        switch(grade / 10){
            case 10:
            case 9:
                System.out.println("A");
                break;
            case 8:
                System.out.println("B");
                break;
            case 7:
                System.out.println("C");
                break;
            case 6:
            case 5:
                System.out.println("D");
                break;
            default:
                System.out.println("F");
        } // end of the switch case
    }
}
