public class WhileExample{
    public static void main(String[] args){
        int counter = 1;
        while(counter <= 10){
            System.out.println(counter);
            counter++;
            //counter = counter + 1;
            //counter += 1;
            //counter -= 1;
            //counter *= 1;
            //counter /= 1;
        }
        
    }
}
